#import libraries
from pyapacheatlas.core import PurviewClient
from pyapacheatlas.auth import ServicePrincipalAuthentication
from pyapacheatlas.readers import ExcelConfiguration, ExcelReader
from pyapacheatlas.core.typedef import AtlasAttributeDef, EntityTypeDef
import json

#purview-test-app is used as service principle
#https://portal.azure.com/#view/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/~/Overview/appId/e680f4c6-1f44-427d-b9bd-4c12060ba005
auth = ServicePrincipalAuthentication(
    tenant_id = "22bf3083-d691-46f4-ab6d-ee707b8687ff", 
    client_id = "e680f4c6-1f44-427d-b9bd-4c12060ba005", 
    client_secret = "tSs8Q~Q1GcXtmQ7JBXRpYSrhtesgf9tKGjDf2bwH"
)

# Create a client to connect to your service.
client = PurviewClient(
    account_name = "PurviewTestJS",
    authentication = auth
)


#create column mapping process
proc_type = EntityTypeDef(
    name = "CMP",
    superTypes = ["Process"],
    attributes = [
        AtlasAttributeDef("columnMapping")
    ]
)

results = client.upload_typedefs(entityDefs = [proc_type], force_update=True)

print(json.dumps(results, indent=2))