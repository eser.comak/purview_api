#import libraries
from pyapacheatlas.core import PurviewClient,AtlasEntity
from pyapacheatlas.auth import ServicePrincipalAuthentication
from pyapacheatlas.readers import ExcelConfiguration, ExcelReader
from pyapacheatlas.core.typedef import AtlasAttributeDef, EntityTypeDef
import json

#purview-test-app is used as service principle
#https://portal.azure.com/#view/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/~/Overview/appId/e680f4c6-1f44-427d-b9bd-4c12060ba005
auth = ServicePrincipalAuthentication(
    tenant_id = "22bf3083-d691-46f4-ab6d-ee707b8687ff", 
    client_id = "e680f4c6-1f44-427d-b9bd-4c12060ba005", 
    client_secret = "tSs8Q~Q1GcXtmQ7JBXRpYSrhtesgf9tKGjDf2bwH"
)

# Create a client to connect to your service.
client = PurviewClient(
    account_name = "PurviewTestJS",
    authentication = auth
)
ts = AtlasEntity(
   name="anotherts",
   typeName="tabular_schema",
   qualified_name="pyapache://anotherdemotabschema",
   guid = -21231
)

# Create a Column entity that references your tabular schema
col01 = AtlasEntity(
  name="democolumn1",
  typeName="column",
  qualified_name="pyapche://mycolumn1",
  attributes={
    "type":"int64",
    "description": "Col1asd"
  },
relationshipAttributes = {
    "composeSchema": ts.to_json(minimum=True)
  }
  )
col02 = AtlasEntity(
  name="democolumn2",
  typeName="column",
  qualified_name="pyapche://mycolumn2",
  attributes={
    "type":"int64",
    "description": "Col2"
  },
  relationshipAttributes = {
    "composeSchema": ts.to_json(minimum=True)
  }
)
colsarray = [col01.to_json(),col02.to_json()]

# Create a resource set that references the tabular schema
rs = AtlasEntity(
  name="manual Purview dataset upload",
  typeName="powerbi_dataset",
  qualified_name="https://app.powerbi.com/groups/15b1f845-e653-45e8-b079-53a91655a107/datasets/307a6bc4-2c02-48f6-a035-3c87dffb5f6e",
  relationshipAttributes = {
    "tabular_schema": ts.to_json(minimum=True)
  }
)

# Upload entities
results = client.upload_entities(
 [rs.to_json(), ts.to_json()]+ colsarray
)
# Print out results to see the guid assignemnts
print(json.dumps(results, indent=2))