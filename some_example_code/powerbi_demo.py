import requests
import msal

APP_ID = 'e680f4c6-1f44-427d-b9bd-4c12060ba005'
TENANT_ID = '22bf3083-d691-46f4-ab6d-ee707b8687ff'
SECRET_VALUE = 'tSs8Q~Q1GcXtmQ7JBXRpYSrhtesgf9tKGjDf2bwH'
authority = f'https://login.microsoftonline.com/{TENANT_ID}'
scopes = ["https://analysis.windows.net/powerbi/api/.default"]

app = msal.ConfidentialClientApplication(APP_ID, authority=authority, client_credential=SECRET_VALUE)

result = app.acquire_token_for_client(scopes=scopes)

if not "access_token" in result:
    print(result.get("error"))
    print(result.get("error_description"))
    print(result.get("correlation_id"))
    raise Exception("Failed to get access token")

url_group_list = 'https://api.powerbi.com/v1.0/myorg/groups'
headers = {
    'Authorization': 'Bearer ' + result['access_token']
}
response = requests.get(url_group_list, headers=headers)
# print(response)

# workspace_list = response.json()['value']
# for workspace in workspace_list:
#     if 'windsor' in workspace['name'].lower():
#         workspaice_id = workspace['id']
#         url_group_delete = f'https://api.powerbi.com/v1.0/myorg/groups/{workspaice_id}'
#         requests.delete(url_group_delete, headers=headers)
#         print(workspace['name'], ' is deleted')
print(response)