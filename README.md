# Purview API

Many of the functionalities available in the Purview UI can also be programmatically accessed through the use of the Purview API. Here are the steps to set up the required access settings and authenticate with the Purview account & instance:

## Getting started

- 1 Go to "App registrations" in the Azure portal
- 2 Create the app with the following settings:
    - 2.1 You don't need to provide Redirect URI for discovery purposes
![image info](images/app_conf.png) 
    - 2.2 Go back to "App registrations" and click your app
    - 2.3 Select "Certificates & secrets" option on the left menu 
    - 2.4 Select Client secrets tab and create a new secret.

- 3 Go to Purview account and select "Access control (IAM)"
    - 3.1 You need to create a role. Click "Add role assignment"
![image info](images/add_role.png) 
    - 3.2 On the "Role" tab select the type of role. Contributor works
![image info](images/select_role.png) 
    - 3.3 On the "Members" tab provide following settings and also the name of the app you created earlier
![image info](images/select_app.png) 
    - 3.4 Click "Review+assign" button. Now your app has access to Purview account. However we still need 
    API access to the actual collections inside of Purview instance

- 4 This step needs to be done by the owner/collection admin of the Purview account
    - 4.1 Click "Open Microsoft Purview Governance Portal" on the overview of Purview account
    - 4.2 Select "Data Map" - > "Collections" -> "Role assignments"
![image info](images/give_acc.png) 
    - 4.2 Click the plus icon near the following roles: Collection admins, Data source admins, Data curators and add your app to the list

## Usage

We are going to use pyapacheatlas python library built on apache atlas framework. For more information vist
- 1 https://pypi.org/project/pyapacheatlas/
- 2 https://datasmackdown.com/
- 3 https://www.youtube.com/@datasmackdown339
- 4 https://wjohnson.github.io/pyapacheatlas-docs/latest/index.html
- 5 https://atlas.apache.org/#/

Install the library

```bash
pip install pyapacheatlas
```


Minimal code to authenticate. Refer to comments for specifics
```python
#import libraries
from pyapacheatlas.core import PurviewClient,AtlasEntity
from pyapacheatlas.auth import ServicePrincipalAuthentication

'''
    tenant_id, client_id, and client_secret can found on the overview tab of the app you created earlier
'''
auth = ServicePrincipalAuthentication(
    tenant_id = "", 
    client_id = "", 
    client_secret = ""
)

client = PurviewClient(
    account_name = "your-purview-account-name",
    authentication = auth
)
```

Get entity definition for easy access

```python
'''
    Returns the dict object for the entity that has the matching guid 
    Guid can be extracted from the URL of the entity such as 
    https://web.purview.azure.com/resource/purview-account-name/main/catalog/entity?
    
    guid=0be76afa-cb37-4c2c-8fba-348bdb5e2c23
    
    &section=details&feature.tenant=22bf3083-d691-46f4-ab6d-ee707b8687ff
'''

client.get_entity(guid="guid-id")
```

Write all type definitions to a file to inspect manually. This action needs to be done regularly to keep track of new additions.
Purview does not provide any documentation on available types. It requires manual work to inspect types.

```python

with open("types.json", "w") as outfile:
    json.dump(client.get_all_typedefs(), outfile, indent=4, sort_keys=False)
```